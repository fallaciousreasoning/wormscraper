import requests
from bs4 import BeautifulSoup

import re

BASE_URL = "https://parahumans.wordpress.com/"
TABLE_OF_CONTENTS = "table-of-contents/"

CHAPTER_PREFIX = '\nNext Chapter\nBrief note from the author:  This story isn’t intended for young or sensitive readers.  Readers who are on the lookout for trigger warnings are advised to give Worm a pass.\n■\n'
CHAPTER_SUFFIX = '\nNext Chapter\n'

PARAGRAPH_BREAK = '\n\n'

class Chapter:
	def __init__(self, url, title='', arc=None):
		self.url = url
		self.title = title
		self.arc = arc
		self.content = ''

	def __str__(self):
		return self.title

	def __repr__(self):
		return str(self)

def get_table_of_contents(url = BASE_URL + TABLE_OF_CONTENTS, html=None):
	print('gettting table of contents...')

	if html == None:
		response = requests.get(url)
		html = response.text

	pattern = r'href="(https://parahumans\.wordpress\.com/category/.*)">(.*?)\<'

	chapters = []

	for url, title in re.findall(pattern, html):
		if title == '': continue

		chapters.append(Chapter(url, title))

	print('done!')
	return chapters

def get_chapters(chapters):
	print('fetching chapters...')

	done = 0.0
	for chapter in chapters:
		get_chapter(chapter)
		done += 1
		print(done / len(chapters))

	print('done!')

def get_chapter(chapter):
	response = requests.get(chapter.url)
	html = response.text

	soup = BeautifulSoup(html, "html.parser")
	title = soup.find('h1', 'entry-title')

	article = soup.find('div', 'entry-content')	

	content = article.text
	if content.startswith(CHAPTER_PREFIX):
		content = content[len(CHAPTER_PREFIX):]

	if content.endswith(CHAPTER_SUFFIX):
		content = content[:-len(CHAPTER_SUFFIX)]

	chapter.title = title.text
	chapter.content = content

	return chapter

def save(filename, title, author, chapters):
	print('saving...')

	with open(filename, 'w', encoding='utf-8') as f:
		f.write('% ' + title + '\n')
		f.write('% ' + author + '\n')

		for chapter in chapters:
			f.write('# ' + chapter.title + '\n\n')
			f.write(chapter.content + '\n\n')

	print('done')

def get_book(output_file='worm'):
	chapters = get_table_of_contents()
	get_chapters(chapters)

	save(output_file + ".pandoc", 'Worm', 'Wildbow', chapters)

get_book()

#get_chapter(Chapter('https://parahumans.wordpress.com/category/stories-arcs-1-10/arc-1-gestation/1-01/', '1.01'))

#print(get_table_of_contents(html=get_test_input()))
